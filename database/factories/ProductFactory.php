<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(5),
            'description' => $this->faker->text(200),
            'image' => $this->faker->imageUrl(480,340,'Product', true),
            'price' => $this->faker->randomFloat(2, 150, 10000),
        ];
    }
}
