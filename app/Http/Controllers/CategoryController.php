<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;

class CategoryController extends Controller
{
    public function admin_category_index() {
        $data = Category::all();
        return view('admin.category', compact('data'));
    }

    public function admin_category_create() {
        return view('admin.categoryCreate');
    }

    public function admin_category_store(CategoryRequest $request) {

        // $validated = $request->validate([
        //     'name' => 'required|unique:categories|max:255|min:2',
        // ]);

        $data = [
            'name' => $request->input('name')
        ];

        Category::create($data);

        return redirect()
            ->route('adminCategoies')
            ->with('message','Successfully Added!');
    }

    public function admin_category_edit($id) {
        $category = Category::find($id);
        return view('admin.categoryEdit', compact('category'));
    }

    public function admin_category_update(CategoryRequest $request, $id) {
        $category = Category::find($id);

        $data = [
            'name' => $request->input('name')
        ];

        $category->update($data);

        return redirect()
            ->route('adminCategoies')
            ->with('message','Successfully Updated!');

    }

    public function admin_category_show($id) {
        $data = Category::find($id);
        return view('admin.categoryShow', compact('data'));
    }

    public function admin_category_delete($id) {
        $data = Category::find($id);
        $data->delete();

        return redirect()
            ->route('adminCategoies')
            ->with('message','Successfully Deleted!');
    }
}
