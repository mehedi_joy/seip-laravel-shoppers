<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

Route::get('/shop', [ProductController::class, 'index'])->name('shop');

Route::get('/shop/{id}', [ItemController::class, 'show'])->name('shopSingle');

Route::get('/contact', function() {
    return view('contact');
})->name('contact');

Route::get('/cart', function() {
    return view('cart');
})->name('cart');

Route::get('/checkout', function() {
    return view('checkout');
})->name('checkout');

Route::get('/orderConfirmed', function() {
    return view('thankYou');
})->name('thankYou');

Route::get('/about', function() {
    return view('about');
})->name('about');

Route::prefix('admin')->group(function () {

    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('adminindex');

    Route::get('/categories', [CategoryController::class, 'admin_category_index'])->name('adminCategoies');
    Route::get('/categories/create', [CategoryController::class, 'admin_category_create'])->name('adminCategoryCreate');
    Route::post('/categories/create', [CategoryController::class, 'admin_category_store'])->name('adminCategoryStore');
    Route::get('/categories/{id}', [CategoryController::class, 'admin_category_show'])->name('adminCategoryShow');
    Route::get('/categories/{id}/edit', [CategoryController::class, 'admin_category_edit'])->name('adminCategoryEdit');
    Route::patch('/categories/{id}', [CategoryController::class, 'admin_category_update'])->name('adminCategoryUpdate');
    Route::get('/categories/{id}/delete', [CategoryController::class, 'admin_category_delete'])->name('adminCategoryDelete');

});
