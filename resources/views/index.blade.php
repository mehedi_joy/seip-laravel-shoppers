<x-master>
    <x-partials.banner/>
    <x-partials.collections/>
    <x-partials.bigSale/>
    <x-partials.featuredProducts/>
</x-master>
