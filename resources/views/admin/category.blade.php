<x-admin>
    @if (session('message'))
    <div class="alert alert-info">
        {{ session('message') }}
    </div>
    @endif
    <a href="{{ route('adminCategoryCreate')}}" class="btn btn-outline-primary mb-4">Create Category</a>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>
                        <a href="{{ route('adminCategoryShow', $category->id) }}" class="btn btn-outline-secondary">Show</a>
                        <a href="{{ route('adminCategoryEdit', $category->id) }}" class="btn btn-outline-info">Edit</a>
                        <a href="{{ route('adminCategoryDelete', $category->id) }}" class="btn btn-outline-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</x-admin>
