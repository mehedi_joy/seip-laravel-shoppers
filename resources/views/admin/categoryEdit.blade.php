<x-admin>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="{{ route('adminCategoryUpdate', $category->id )}}" method="post" style="width: 30vw;margin: auto;">
        @csrf
        @method('patch')
        <div class="form-group">
            <label class="form-label" for="name">Name:</label>
            <input class="form-control" type="text" name="name" id="name" value="{{ $category->name }}">
        </div>
        <input class="btn btn-primary" type="submit" value="Save">
    </form>
</x-admin>
