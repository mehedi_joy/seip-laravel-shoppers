<x-admin>
    <form style="width: 30vw;margin: auto;">
        <label class="form-label" for="category_id">ID:</label>
        <input type="text" class="form-control" name="category" id="category_id" value="{{ $data->id }}" disabled>
        <label class="form-label" for="category_name">Name:</label>
        <input type="text" class="form-control" name="category" id="category_name" value="{{ $data->name }}" disabled>
    </form>
</x-admin>
